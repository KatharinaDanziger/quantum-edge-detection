# Quantum Edge Detection

Implementation and testing of the Hadamard edge detection algorithm with an
auxiliary (ancilla) qubit. For more details, see 
https://arxiv.org/pdf/1801.01465.pdf.

Exemplary Quantum Circuit for a four qubit (16 pixels) encoded image. The 
unitary gate corresponds to a decrement gate:
![Example Circuit Edge Detection](images/example_quantum_circuit.jpg)

Example results of the implemented Hadamard edge detection algorithm:
![Example Quantum Edge Detection](images/example_quantum_edge_qubit.jpg)



## Installation
You can simply run 
```commandline
make venv
```
to create a virtual environment and install the local 
`quantum_edge_detector` package.


## Work in progress
... 🙂 ...
