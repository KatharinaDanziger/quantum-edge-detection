# -*- coding: utf-8 -*-
"""
Hadamard quantum edge detection implementation.

For more details on Hadamard quantum edge detection, see
https://arxiv.org/pdf/1801.01465.pdf.
"""

from itertools import product

import numpy as np
from qiskit import QuantumCircuit
from qiskit_aer import Aer

from .image_utils import sliding_window_view


class HadamardEdgeDetector:
    """Variation of quantum Hadamard edge detection with auxiliary qubit.

    Attributes:
        n_dim: The dimension. Is always 2 (for 2-dim images).
        window_size: The window size by which the original image is divided
        window_shape: The window shape derived from `window_size` and `n_dim`.
        n_data_qubits: The number of qubits needed to encode the image. Here
            the number is derived from the window size by which the original
            image is divided.
        n_ancilla_qubits: The number of ancilla qubits. Is always 1.
        n_qubits: The total number of needed qubits. Is the sum of
            `n_data_qubits` and `n_ancilla_qubits`.
        backend: The backend where to perform quantum computations.
        shots: The number of times the quantum circuit is sampled to obtain
            measurement results.
        D2n_1: The amplitude permutation unitary, derived from `n_qubits`.

    """

    def __init__(self, window_size: int, backend: str, shots: int):
        """Initialization."""
        self.n_dim = 2  # 2-dim images (could be extended to 3-dim..)

        # quantum circuit parameters
        self.window_size = window_size
        self.window_shape = (window_size,) * self.n_dim
        # paper: need n=log_2(N) = log_2(M*L) qubit for image with N=M*L pixels
        # here image is divided in multiple parts of size
        # (`window_size` x `window_size`) which are combined later on
        self.n_data_qubits = int(np.log2(self.window_size**self.n_dim))
        # additional ancilla (auxiliary) qubit to perform computation on both
        # even- and odd-pixel-pairs simultaneously
        self.n_ancilla_qubits = 1
        self.n_qubits = self.n_data_qubits + self.n_ancilla_qubits
        self.backend = backend
        self.shots = shots
        # initialize amplitude permutation unitary, to transform amplitudes
        # into structure which will make image gradients calculation easier
        # (corresponds to decrement gate)
        self.D2n_1 = np.roll(np.identity(2**self.n_qubits), 1, axis=1)

    def run_window_edge_scan(
        self,
        window: np.ndarray,
        threshold: float = 1e-3,
    ) -> np.ndarray:
        """Run the edge detection algorithm for a window box of the image.

        Args:
            window: The input data window.
            threshold: The threshold to be considered. Measures how big the
                gradients needs to be such that it is considered an edge.
                Defaults to 1e-3.

        Returns:
            The edge scanned input window.
        """
        # flatten the input window
        window = window.flatten()

        # quantum circuit initialization
        qc = self.initialize_quantum_circuit(window)

        # apply quantum operations for edge detection
        qc = self.apply_hadamard_edge_detection(qc)

        # run quantum circuit on Aer simulator backend
        counts = self.run_quantum_circuit(qc)

        # retrieve state vector from counts:
        # calculate binary string of basis qubits and the ordered state vector
        new_counts = [
            counts.get(
                "".join(map(str, q_bits)), 0
            )  # get counts for string 'q_bits', default to 0 if unavailable
            for q_bits in product([0, 1], repeat=self.n_qubits)
        ]
        # convert `new_counts` to an array and normalize to obtain state vector
        state_vector = np.array(new_counts) / np.sum(new_counts)

        # extract odd values from state vector for edge detection
        final_state = state_vector[range(1, 2 ** (self.n_data_qubits + 1), 2)]

        # detect edges based on `threshold`
        edge_scan = np.zeros(final_state.shape)
        edge_scan[np.abs(final_state) > threshold] = 1

        # reshape to window box shape
        edge_scan_result = edge_scan[: self.window_size**self.n_dim].reshape(
            self.window_shape
        )
        return edge_scan_result

    def initialize_quantum_circuit(
        self,
        image_window: np.ndarray,
    ) -> QuantumCircuit:
        """Initialize the Quantum circuit.

        Args:
            image_window: The image window data to run quantum operations on.

        Returns:
            The initialized Quantum circuit.
        """
        # create Quantum circuit
        quantum_circuit = QuantumCircuit(self.n_qubits)
        # initialize the state vector corresponding to the image window box
        # image window box consists of 2^n pixels with n being number qubits
        initial_state = np.zeros(2**self.n_data_qubits)
        initial_state[: image_window.shape[0]] = image_window
        # normalize `initial_state` and initialize the Quantum circuit
        initial_state = initial_state / np.linalg.norm(initial_state)
        quantum_circuit.initialize(initial_state, range(1, self.n_qubits))
        return quantum_circuit

    def apply_hadamard_edge_detection(
        self,
        quantum_circuit: QuantumCircuit,
    ) -> QuantumCircuit:
        """Apply the Quantum operations for Hadamard edge detection algorithm
        with auxiliary qubit.

        Args:
            quantum_circuit: The Quantum circuit.

        Returns:
            The Quantum circuit for Hadamard edge detection.
        """
        # 1. Hadamard gate
        quantum_circuit.h(0)
        # 2. Decrement gate
        quantum_circuit.unitary(self.D2n_1, range(self.n_qubits))
        # 3. Hadamard gate
        quantum_circuit.h(0)
        quantum_circuit.measure_all()
        return quantum_circuit

    def run_quantum_circuit(self, quantum_circuit: QuantumCircuit) -> dict:
        """Run the Quantum circuit on Aer simulator backend.

        Args:
            quantum_circuit: The Quantum circuit.

        Returns:
            The resulting counts of the simulation run.
        """
        # get Aer simulator backend
        simulator = Aer.get_backend(self.backend)
        # run simulation and get counts
        counts = (
            simulator.run(
                quantum_circuit,
                shots=self.shots,
            )
            .result()
            .get_counts()
        )
        return counts

    def _process_window(
        self,
        window: np.ndarray,
        threshold: float,
    ) -> np.ndarray:
        """Process window for edge detection.

        Args:
            window: The window data for edge detection.
            threshold: The threshold for edge detection.

        Returns:
            Edge detection result for the window.
        """
        if np.sum(window) > threshold:
            return self.run_window_edge_scan(window, threshold)
        else:
            return np.zeros(self.window_shape)

    def run_image_edge_scan(
        self,
        image: np.ndarray,
        threshold: float = 1e-3,
    ):
        """Run the edge detection algorithm for the entire image.

        Args:
            image: The input image data to scan for edges.
            threshold: The threshold to be considered. Measures how big the
                gradient s to be such that it is considered an edge.
                Defaults to 1e-3.

        Returns:
            The edge scanned input image.
        """
        # get sliding window view of the input image
        shape, windows = sliding_window_view(
            image,
            (self.window_size,) * self.n_dim,  # type: ignore[arg-type]
            dx=self.window_size,
            dy=self.window_size,
        )

        # reshape windows for iteration
        windows = windows.reshape((-1,) + (self.window_size,) * self.n_dim)

        # obtain edge detection results for each image window box
        results = [
            self._process_window(windows[i], threshold)
            for i in range(windows.shape[0])
        ]

        # reshape the results to the original image shape
        results = np.array(results).reshape(shape)  # type: ignore[assignment]
        transposition_map = {
            4: [0, 2, 1, 3],
            5: [0, 1, 3, 2, 4],
            6: [0, 3, 1, 4, 2, 5],
            7: [0, 1, 4, 2, 5, 3, 6],
            8: [0, 1, 2, 5, 3, 6, 4, 7],
        }
        edge_scanned_image = results.transpose(  # type: ignore[attr-defined]
            transposition_map[len(results.shape)]  # type: ignore[attr-defined]
        ).reshape(image.shape)

        # return the reverted edge scanned image
        return edge_scanned_image

    def edge_scan(
        self,
        image: np.ndarray,
        threshold: float = 1e-3,
        horizontal: bool = True,
        vertical: bool = True,
    ):
        """Run the edge detection algorithm for the entire image.

        Args:
            image: The input image data to scan for edges.
            threshold: The threshold to be considered. Measures how big the
                gradient s to be such that it is considered an edge.
                Defaults to 1e-3.
            horizontal: Whether to scan for edges horizontally.
                Defaults to True.
            vertical: Whether to scan for edges vertically. Defaults to True.

        Returns:
            The edge scanned input image.
        """
        edges_horizontal_scan = np.zeros_like(image)
        edges_vertical_scan = np.zeros_like(image)

        # scan for edges horizontally
        if horizontal:
            edges_horizontal_scan = self.run_image_edge_scan(image, threshold)

        # scan for edges vertically
        if vertical:
            # transpose image for vertical scan
            transposed_image = np.transpose(image, (0, 2, 1))
            vertical_edges = self.run_image_edge_scan(
                transposed_image,
                threshold,
            )
            # transpose again to retrieve original shape
            edges_vertical_scan = np.transpose(vertical_edges, (0, 2, 1))

        # combine edges from horizontal and vertical scan
        image_edges = np.logical_or(edges_horizontal_scan, edges_vertical_scan)
        return image_edges.astype(int)
