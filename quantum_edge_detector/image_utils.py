# -*- coding: utf-8 -*-
"""Useful image-related utility functions."""

import numpy as np
from cv2 import COLOR_BGR2GRAY, cvtColor
from numpy.lib.stride_tricks import as_strided


def sliding_window_view(
    input_array: np.ndarray,
    window_shape: tuple[int, int],
    dx: int = 1,
    dy: int = 1,
) -> tuple[tuple[int, ...], np.ndarray]:
    """Create a sliding/ rolling window view of an input array based on a
    specified window size.

    Args:
        input_array: The input array (image) to create the sliding window view
            from.
        window_shape: The window shape.
        dx: The horizontal step size. Defaults to 1.
        dy: The vertical step size. Defaults to 1.

    Returns:
        A tuple containing the shape of the rolling window view and the sliding
            window view of the input array.

    Raises:
        ValueError: If any calculated dimension is invalid (<=0).
    """
    # check if `input_array` size is the same as `window_shape`
    if input_array.shape == window_shape:
        return (1, 1, *input_array.shape), input_array

    # capture shape and strides of input array
    input_shape = input_array.shape
    input_strides = input_array.strides

    # initialize shape and strides for the sliding window view
    shape = input_shape[:-2]
    strides = input_strides[:-2]

    # calculate dimensions for sliding window view based on step & input sizes
    shape += (
        ((input_shape[-2] - window_shape[-2]) // dy + 1),
        ((input_shape[-1] - window_shape[-1]) // dx + 1),
        *window_shape,
    )
    strides += (input_strides[-2] * dy,) + (input_strides[-1] * dx,)
    strides += input_strides[-2:]

    # check if any calculated dimensions are invalid (<= 0)
    if any(s <= 0 for s in shape):
        raise ValueError(f"Invalid window or step sizes: {shape=}.")

    # create the sliding window view and return it together with the `shape`
    sliding_view = as_strided(input_array, shape=shape, strides=strides)
    return shape, sliding_view


def convert_to_grayscale(image: np.ndarray) -> np.ndarray:
    """Convert an image array to grayscale

    Args:
        image: The input image.

    Returns:
        The grayscale image.

    Raises:
        ValueError: If `image` has an invalid shape.
    """
    # check if image is already grayscale
    if len(image.shape) == 2:
        return image
    # check if image is a colored (RGB) image
    elif len(image.shape) == 3 and image.shape[2] == 3:
        return cvtColor(image, COLOR_BGR2GRAY)
    # otherwise (unsupported image format) raise ValueError
    else:
        raise ValueError(f"Image shape is not valid: {image.shape}.")
