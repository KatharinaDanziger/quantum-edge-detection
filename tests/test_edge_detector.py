# -*- coding: utf-8 -*-
"""Tests for HadamardEdgeDetector class."""

import numpy as np
import pytest

from quantum_edge_detector.edge_detector import HadamardEdgeDetector


@pytest.fixture
def edge_detector():
    window_size = 2
    backend = "aer_simulator"
    shots = 2 ** 10
    return HadamardEdgeDetector(window_size, backend, shots)


def test_hadamard_edge_detector_init():
    """Test `HadamardEdgeDetector` class initialization."""
    # parameters for initialization
    window_size = 4
    backend = "aer_simulator"
    shots = 2**10

    # initialize the `HadamardEdgeDetector object
    edge_detector = HadamardEdgeDetector(window_size, backend, shots)

    # check whether attributes are set correctly after initialization
    assert edge_detector.n_dim == 2
    assert edge_detector.window_size == window_size
    assert edge_detector.backend == backend
    assert edge_detector.shots == 1024

    # validate derived attributes based on initialization parameters
    assert edge_detector.window_shape == (window_size, window_size)
    assert edge_detector.n_data_qubits == int(np.log2(window_size ** 2))
    assert edge_detector.n_qubits == int(np.log2(window_size ** 2)) + 1


def test_run_window_edge_scan_no_edges(edge_detector):
    """
    Test `HadamardEdgeDetector` class `run_window_edge_scan` method with window
    containing no edges.
    """
    # test with a window containing no edges (low gradient values)
    window_no_edges = np.array([[0.01, 0.02], [0.03, 0.04]])
    edge_scan_no_edges = edge_detector.run_window_edge_scan(
        window_no_edges,
        threshold=1e-1,
    )
    # check if no edges are detected
    assert np.sum(edge_scan_no_edges) == 0


def test_run_window_edge_scan_minimal_gradients(edge_detector):
    """
    Test `HadamardEdgeDetector` class `run_window_edge_scan` method with window
    containing extremely low gradients.
    """
    # test with a window containing extremely low gradient values
    window_minimal_gradients = np.array([[1e-7, 2e-7], [3e-7, 4e-7]])
    edge_scan_minimal_gradients = edge_detector.run_window_edge_scan(
        window_minimal_gradients, threshold=1e-1
    )
    # check if no edges are detected
    assert np.sum(edge_scan_minimal_gradients) == 0


def test_run_window_edge_scan_clear_edges(edge_detector):
    """
    Test `HadamardEdgeDetector` class `run_window_edge_scan` method with window
    containing clear edges.
    """
    # test with a window containing clear edges (high gradient values)
    window_clear_edges = np.array([[0.9, 0.1], [0.1, 0.9]])
    edge_scan_clear_edges = edge_detector.run_window_edge_scan(
        window_clear_edges
    )
    # check if edges are detected
    assert np.sum(edge_scan_clear_edges) > 0


def test_run_window_edge_scan_edges_near_threshold(edge_detector):
    """
    Test `HadamardEdgeDetector` class `run_window_edge_scan` method with window
    containing edges near the threshold.
    """
    # test with a window containing edges near the threshold
    window_near_threshold = np.array([[0.001, 0.002], [0.003, 0.004]])
    edge_scan_near_threshold = edge_detector.run_window_edge_scan(
        window_near_threshold, threshold=1e-2)
    # check if edges are detected near the threshold
    assert 0 < np.sum(edge_scan_near_threshold) <= np.prod(
        window_near_threshold.shape
    )


def test_run_window_edge_scan_too_large_window(edge_detector):
    """
    Test `HadamardEdgeDetector` class `run_window_edge_scan` method with a too
    large window size.
    """
    # test with a too large window size (should raise ValueError)
    # create a random 5x5 window
    window_large_size = np.random.rand(5, 5)
    with pytest.raises(ValueError):
        edge_detector.run_window_edge_scan(
            window_large_size
        )
