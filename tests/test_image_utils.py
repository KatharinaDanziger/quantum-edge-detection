# -*- coding: utf-8 -*-
"""Tests for image-related utility functions."""

import numpy as np
import pytest

from quantum_edge_detector.image_utils import (
    convert_to_grayscale,
    sliding_window_view,
)


def test_sliding_window_view():
    """Test `sliding_window_view` function."""
    # create sample input array and window size
    input_array = np.array([[1, 2, 3], [4, 5, 6], [7, 8, 9]])
    window_shape = (2, 2)

    # expected output shape of the sliding window view
    expected_shape = (2, 2, 2, 2)

    shape, view = sliding_window_view(input_array, window_shape)

    # check if shape of sliding window view matches the expected shape
    assert shape == expected_shape

    # check if content of the views is as expected
    first_window = view[0, 0]
    expected_first_window = np.array([[1, 2], [4, 5]])
    assert np.array_equal(first_window, expected_first_window)
    second_window = view[0, 1]
    expected_second_window = np.array([[2, 3], [5, 6]])
    assert np.array_equal(second_window, expected_second_window)
    third_window = view[1, 0]
    expected_third_window = np.array([[4, 5], [7, 8]])
    assert np.array_equal(third_window, expected_third_window)
    fourth_window = view[1, 1]
    expected_fourth_window = np.array([[5, 6], [8, 9]])
    assert np.array_equal(fourth_window, expected_fourth_window)

    # non-quadratic input
    input_array_2 = np.array([[1, 2, 3], [4, 5, 6], [7, 8, 9], [10, 11, 12]])
    window_shape_2 = (2, 2)
    expected_shape_2 = (3, 2, 2, 2)
    shape_2, view_2 = sliding_window_view(input_array_2, window_shape_2)
    assert shape_2 == expected_shape_2
    window = view_2[0, 0]
    expected_window = np.array([[1, 2], [4, 5]])
    assert np.array_equal(window, expected_window)
    window = view_2[0, 1]
    expected_window = np.array([[2, 3], [5, 6]])
    assert np.array_equal(window, expected_window)
    window = view_2[1, 0]
    expected_window = np.array([[4, 5], [7, 8]])
    assert np.array_equal(window, expected_window)
    window = view_2[1, 1]
    expected_window = np.array([[5, 6], [8, 9]])
    assert np.array_equal(window, expected_window)
    window = view_2[2, 0]
    expected_window = np.array([[7, 8], [10, 11]])
    assert np.array_equal(window, expected_window)
    window = view_2[2, 1]
    expected_window = np.array([[8, 9], [11, 12]])
    assert np.array_equal(window, expected_window)


def test_sliding_window_view_non_default_step_sizes():
    """Test `sliding_window_view` function with non default step sizes."""
    # create sample input array and window size
    input_array = np.array([[1, 2, 3], [4, 5, 6], [7, 8, 9]])
    window_shape = (1, 1)

    # dx=2, dy=1
    expected_shape = (3, 2, 1, 1)
    shape, view = sliding_window_view(input_array, window_shape, dx=2, dy=1)
    assert shape == expected_shape
    window = view[0, 0]
    expected_window = np.array([[1]])
    assert np.array_equal(window, expected_window)
    window = view[0, 1]
    expected_window = np.array([[3]])
    assert np.array_equal(window, expected_window)
    window = view[1, 0]
    expected_window = np.array([[4]])
    assert np.array_equal(window, expected_window)
    window = view[1, 1]
    expected_window = np.array([[6]])
    assert np.array_equal(window, expected_window)
    window = view[2, 0]
    expected_window = np.array([[7]])
    assert np.array_equal(window, expected_window)
    window = view[2, 1]
    expected_window = np.array([[9]])
    assert np.array_equal(window, expected_window)

    # dx=1, dy=2
    expected_shape = (2, 3, 1, 1)
    shape, view = sliding_window_view(input_array, window_shape, dx=1, dy=2)
    assert shape == expected_shape
    window = view[0, 0]
    expected_window = np.array([[1]])
    assert np.array_equal(window, expected_window)
    window = view[0, 1]
    expected_window = np.array([[2]])
    assert np.array_equal(window, expected_window)
    window = view[0, 2]
    expected_window = np.array([[3]])
    assert np.array_equal(window, expected_window)
    window = view[1, 0]
    expected_window = np.array([[7]])
    assert np.array_equal(window, expected_window)
    window = view[1, 1]
    expected_window = np.array([[8]])
    assert np.array_equal(window, expected_window)
    window = view[1, 2]
    expected_window = np.array([[9]])
    assert np.array_equal(window, expected_window)

    # dx=2, dy=2
    expected_shape = (2, 2, 1, 1)
    shape, view = sliding_window_view(input_array, window_shape, dx=2, dy=2)
    assert shape == expected_shape
    window = view[0, 0]
    expected_window = np.array([[1]])
    assert np.array_equal(window, expected_window)
    window = view[0, 1]
    expected_window = np.array([[3]])
    assert np.array_equal(window, expected_window)
    window = view[1, 0]
    expected_window = np.array([[7]])
    assert np.array_equal(window, expected_window)
    window = view[1, 1]
    expected_window = np.array([[9]])
    assert np.array_equal(window, expected_window)


def test_rolling_window_view_invalid_input():
    """
    Test `sliding_window_view` function raise ValueError for invalid input.
    """
    # test case for invalid input:
    # window size is larger than the input array
    input_array = np.array([[1, 2], [3, 4]])
    window_shape = (2, 3)

    # assert raised ValueError when the window size is invalid
    with pytest.raises(ValueError):
        sliding_window_view(input_array, window_shape)


def test_convert_to_grayscale_grayscale_image():
    """Test `convert_to_grayscale` function with grayscale image."""
    # Create a sample grayscale image
    grayscale_image = np.array([[100, 150, 200], [50, 75, 25]])

    # Convert the grayscale image to grayscale using the function
    result_grayscale = convert_to_grayscale(grayscale_image)

    # Check if the result matches the original grayscale image
    assert np.array_equal(result_grayscale, grayscale_image)


def test_convert_to_grayscale_rgb_image():
    """Test `convert_to_grayscale` function with RGB image."""
    # create a sample color (RGB) image
    color_image = np.random.randint(0, 256, size=(4, 4, 3), dtype=np.uint8)

    # convert color image to grayscale
    result_color_to_gray = convert_to_grayscale(color_image)

    # check if shape of the resulting grayscale image is correct
    assert len(result_color_to_gray.shape) == 2


def test_invalid_image_shape():
    """
    Test `convert_to_grayscale` function raise ValueError with invalid input
    image shape.
    """
    # check if the function raises ValueError for an invalid image shape
    with pytest.raises(ValueError):
        # creating an invalid image shape
        invalid_image = np.zeros((4, 4, 2))
        convert_to_grayscale(invalid_image)
